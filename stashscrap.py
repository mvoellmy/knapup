from lxml import html
import requests

# chcp 65001

wear_dict = {0: "StatTrak Factory New",
			 1: "StatTrak Minimal Wear",
			 2: "StatTrak Field-Tested",
			 3: "StatTrak Well-Worn",
			 4: "StatTrak Battle-Scarred",
			 5: "Factory New",
			 6: "Minimal Wear",
			 7: "Field-Tested",
			 8: "Well-Worn",
			 9: "Battle-Scarred"}

dict_wear = {0: "StatTrak Factory New",
			 1: "StatTrak Minimal Wear",
			 2: "StatTrak Field-Tested",
			 3: "StatTrak Well-Worn",
			 4: "StatTrak Battle-Scarred",
			 5: "Factory New",
			 6: "Minimal Wear",
			 7: "Field-Tested",
			 8: "Well-Worn",
			 9: "Battle-Scarred"}

grade_list = ["Covert",
			  "Classified",
			  "Restricted",
			  "Mil-Spec"]

knifes = ['Bayonet', 'Butterfly Knife', 'Falchion Knife', 'Flip Knife', 'Gut Knife', 'Huntsman Knife', 'Karambit','M9 Bayonet', 'Shadow Daggers', 'Bowie Knife']

class case():
	def __init__(self, name, link):
		print("{0} Opened".format(name))
		
		self.name = name
		self.link = link

		self.page = requests.get(self.link)
		self.tree = html.fromstring(self.page.content)
		
		self.weapons = self.get_weapons()
		print("The {0} contains {1} weapons.".format(self.name, len(self.weapons)))


	def get_weapons(self):
		_weapons = []
		weapon_names = []

		weapon_grades = self.tree.xpath("/html/body/div[2]/div[5]/div[@class='col-lg-4 col-md-6 col-widen text-center']/div/div[1]/p/text()")
		
		# /html/body/div[2]/div[5]/div[1]/div/div[1]

		# print(weapon_grades)

		if weapon_grades[0] == 'Covert Knife':
			weapon_skins = 	self.tree.xpath("/html/body/div[2]/div[5]/div[@class='col-lg-4 col-md-6 col-widen text-center']/div/h3/a/text()")
			# weapon_skins = weapon_skins[0].replace("\u2605", "")
			for i in range(len(weapon_grades)):
				weapon_names.append(self.name)

		else:
			weapon_skins = 	self.tree.xpath("/html/body/div[2]/div[5]/div[@class='col-lg-4 col-md-6 col-widen text-center']/div/h3/a[2]/text()")
			weapon_names = 	self.tree.xpath("/html/body/div[2]/div[5]/div[@class='col-lg-4 col-md-6 col-widen text-center']/div/h3/a[1]/text()")
			
		weapon_links = 		self.tree.xpath("/html/body/div[2]/div[5]/div[@class='col-lg-4 col-md-6 col-widen text-center']/div/a/@href")


		weapon_stattracks = self.tree.xpath("/html/body/div[2]/div[5]/div[@class='col-lg-4 col-md-6 col-widen text-center']/div/div[2]/p/text()")

		# print(weapon_stattracks)
		# print(weapon_names)
		# print(weapon_links)
		# print(weapon_skins)

		for i in range(len(weapon_grades)):
			# print(weapon_names[i] + ' | ' + weapon_skins[i])
			if weapon_stattracks[i]:
				obj = weapon(weapon_names[i], weapon_links[i], weapon_skins[i], weapon_grades[i], weapon_stattracks[i])
				_weapons.append(obj)
			elif not weapon_stattracks[i]:
				obj = weapon(weapon_names[i], weapon_links[i], weapon_skins[i], weapon_grades[i])
				_weapons.append(obj)

		return _weapons



class weapon():
	def __init__(self, name, link, skin, grade, stattrack=False):

		self.name = name
		self.link = link
		self.skin = skin
		self.grade = grade
		self.stattrack = stattrack



		self.page = requests.get(self.link)
		self.tree = html.fromstring(self.page.content)

		self.steam_prices = self.get_prices('prices') 		# STEAM
		self.opskins_prices = self.get_prices('opskins')	# OPSKINS

		self.min_wear = self.tree.xpath("/html/body/div[2]/div[2]/div[2]/div/div[4]/div/div/div/div[1]/div[1]/div[1]/div/text()")
		self.max_wear = self.tree.xpath("/html/body/div[2]/div[2]/div[2]/div/div[4]/div/div/div/div[1]/div[2]/div[1]/div/text()")

		self.price_ratios = self.get_price_ratios()

		self.set_max_price_ratio()

		self.profit = 0
		if self.max_price_ratio_key > -1:
			self.profit = 0.85*float(self.steam_prices[self.max_price_ratio_key].replace(" CHF",""))*0.85 - float(self.opskins_prices[self.max_price_ratio_key].replace(" CHF",""))


	def get_prices(self, price_source):
		
		price_list = []

		prices_xpath		= "//*[@id='{0}']/div[@class='btn-group-sm btn-group-justified']/a/span[@class='pull-right']/text()".format(price_source) 
		wear_xpath			= "//*[@id='{0}']/div[@class='btn-group-sm btn-group-justified']/a/span[@class='pull-left']/text()".format(price_source) 
		stattracks_xpath	= "//*[@id='{0}']/div[@class='btn-group-sm btn-group-justified']/a/span[@class='pull-left price-details-st']/text()".format(price_source)

		prices		= self.tree.xpath(prices_xpath)
		wear		= self.tree.xpath(wear_xpath)
		stattracks 	= self.tree.xpath(stattracks_xpath)

		for i in range(len(prices)):
			price_list.append(prices[i])

		price_list = [price.replace("'", "") for price in price_list]

		return price_list

	def get_price_ratios(self):
		_price_ratios = []
		for steam_price, opskins_price in zip(self.steam_prices, self.opskins_prices):
			if steam_price == 'No Recent Price' or steam_price == 'Not Possible' or opskins_price == 'No Recent Price' or opskins_price == 'Not Possible' :
				_price_ratios.append(-1)
			else:

				steam_price = steam_price.replace(" CHF","")
				opskins_price = opskins_price.replace(" CHF", "")

				steam_price = float(steam_price)
				opskins_price = float(opskins_price)

				# Use if you buy from OP and sell on steam
				# _price_ratios.append(steam_price/opskins_price)
				# Use if you buy from steam and sell on OP
				_price_ratios.append(steam_price/opskins_price)


		return _price_ratios

	def set_max_price_ratio(self):
		self.max_price_ratio = 0
		self.max_price_ratio_key = -1
		for i in range(len(self.price_ratios)):
			if self.price_ratios[i] > self.max_price_ratio:
				self.max_price_ratio = self.price_ratios[i]
				self.max_price_ratio_key = i

def getKey(obj):
	#return obj.max_price_ratio
	return obj.profit

class moneyMachine():
	def __init__(self):
		self.all_weapons = []

		self.page = requests.get("https://csgostash.com/containers/skin-cases")
		self.tree = html.fromstring(self.page.content)

		self.cases = self.get_cases()
		self.run()

	def get_cases(self):
		_cases = []
		cases_links = self.tree.xpath("/html/body/div[2]/div[5]/div[@class='col-lg-4 col-md-6 col-widen text-center']/div/a/@href")
		cases_names = self.tree.xpath("/html/body/div[2]/div[5]/div[@class='col-lg-4 col-md-6 col-widen text-center']/div/a/h4/text()")


		# All Sticker Capsules

		# All Colections
		# All Cases
		# for i in range(len(cases_links)):
		#	_cases.append(case(cases_names[i], cases_links[i]))

		# # All Knifes
		for knife in knifes:
			knife_link = "https://csgostash.com/weapon/{0}".format(knife.replace(" ","+"))
			_cases.append(case(knife, knife_link))

		# Single Case for Testing
		# _cases.append( case("Operation Wildfire Case","https://csgostash.com/case/112/Operation-Wildfire-Case"))

		return _cases

	def run(self):
		for case in self.cases:
			for weapon in case.weapons:
				self.all_weapons.append(weapon)


		self.all_weapons_sorted = sorted(self.all_weapons, key=getKey)

		# file_stream = open('./results/results_ratio.txt','w')
		file_stream = open('./results/results_profit.txt','w')

		for weapon in self.all_weapons_sorted:
			if weapon.max_price_ratio_key > -1:
				file_stream.write("################################################################################\n")
				file_stream.write("Price Ratio: {0:.2f} \nWeapon: {1} {2}\nLink:    {3}\n------------------\n".format(weapon.max_price_ratio, wear_dict[weapon.max_price_ratio_key], weapon.name, weapon.link))
				#file_stream.write("OP Price: {0:.2f} \nSteam Price: {1}".format(weapon.max_price_ratio, wear_dict[weapon.max_price_ratio_key], weapon.name, weapon.link))
				file_stream.write("Estimated profit: CHF {0:.2f}\n".format(weapon.profit))
			else:
				file_stream.write("################################################################################\n")
				file_stream.write("{0} {1} not on both marketplaces available.\n".format(weapon.name, weapon.skin))


		file_stream.close()

# def get_tradeups(self, weapons):
# 	grade_values[8]
# 	wear_values[10]
# 	grade_counter = 0;
	
# 	for weapon in weapons:		
# 		for grade in grades_list:
# 			if weapon.grade == grade:
# 				wear_values[10];
# 				for wear in wear_list:
# 					# Calculate average weapon prices of different rarities


		# wear_costs[10]
			# for wear in wear_dict:
			# Calculate lowest weapon costs

		# Create Table with all trade ups with ratio, profit and links





money = moneyMachine()