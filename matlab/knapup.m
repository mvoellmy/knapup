%%%%% ADAPTED FOR TRADEUP
clear all;
close all;
clc;
%% Read File
% weapons(1): Position on Steam market
% weapons(2): Weapon wear
% weapons(3): Price ($)
% weapons(4): Listing ID


filename = 'csv/shadow/pink.csv';
fileID = fopen(filename);

weapons_cells = textscan(fileID, '%f%f%f%f%s%f%s%s%s%s%s',2000,'delimiter',',');
weapons_cells(:,[5,6,7,8,9,10,11])=[];
weapons = cell2mat(weapons_cells);

%%
% weapons used in tradup
n = 20;

A = weapons(:,2)'; % wear of each item
f = weapons(:,3)'; % price of each item
b = 0.7;           % limiting wear of the knapup

% Setting Knapup item number to 10
Aeq = ones(size(f));
beq = n;

% General code specifying the lower bound is 0 and upper bound is 1
lb = zeros(size(f));
ub = ones(size(f));

% General code specifying all variables are integer variables
intvars = 1:length(f);

[x,fval,exitflag,output] = intlinprog(f,intvars,A,b,Aeq,beq,lb,ub);

if exitflag > 0
    % Getting indizes of tradeup weapons
    k = find(x);

    tu_weapons = zeros(n,4);

    for i=1:n
        tu_weapons(i,:) = weapons(k(i),:);
    end

    total_price = fval;
    total_wear = mean(tu_weapons(:,2));

    fprintf('Total Price:    %f $\nTotal Wear:      %f\n',fval, total_wear)
   
    fprintf('Average Return: %f\n',mean([57]*n/10)*.85)
    
    fprintf('Buy the following weapons:\n')
        fprintf('Pos:     Wear:         Price:\n')
        fprintf('---------------------------------------\n')
    for i=1:n
        fprintf('%f',tu_weapons(i, 1))
        
        if tu_weapons(i, 2) > 0.07
        fprintf('   MW %f',tu_weapons(i,2))
        else
        fprintf('   FN %f',tu_weapons(i,2))
        end
        
        fprintf('   %f\n',tu_weapons(i, 3))
        
        fprintf('---------------------------------------\n')
        
    end
else
    fprintf('No feasible solution was found.\n')
end
