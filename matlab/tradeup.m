clear all

mw_sorted = get_sorted_items('csv/mw_2.csv');
fn_sorted = get_sorted_items('csv/fn_2.csv');

number_of_tu = 1;

quan_mw = number_of_tu*6;
quan_fn = number_of_tu*10 - quan_mw;

price_mw = mean(mw_sorted(1:quan_mw,3));
price_fn = mean(fn_sorted(1:quan_fn,3));

price_tradeup = (price_mw * quan_mw + price_fn * quan_fn);

float_mw = mean(mw_sorted(1:quan_mw,2));
float_fn = mean(fn_sorted(1:quan_fn,2));

float_tradeup = (quan_mw*float_mw + float_fn*quan_fn)/(quan_mw + quan_fn);

tradup_pricerange = [278, 217]*0.85*number_of_tu;

value_tradeup = mean(tradup_pricerange);
if float_tradeup < 0.07
    fprintf('-------------------------------------------------------------\n')
    fprintf('Float FN:                                         %f\n', float_fn)
    fprintf('Float MW:                                         %f\n', float_mw)
    fprintf('Float TU:                                         %f\n', float_tradeup)
    fprintf('-------------------------------------------------------------\n')
    fprintf('The items for the tradeup cost                    %f $ \n', price_tradeup)
    fprintf('Average value of received tradeup item:           %f $\n', value_tradeup)
    fprintf('-------------------------------------------------------------\n')
    fprintf('PROFIT:                                           %f $\n', value_tradeup - price_tradeup)
else
    fprintf('-------------------------------------------------------------\n')
    fprintf('Float FN:                                         %f\n', float_fn)
    fprintf('Float MW:                                         %f\n', float_mw)
    fprintf('Float of the Tradeup is to high :(                %f\n', float_tradeup)
end

% print_IDs = true;
print_IDs = false;

if print_IDs == true
    fprintf('-------------------------------------------------------------\n')
    fprintf('Factory New Weapons:\n')
    for i = 1:quan_fn
        fprintf('ID:  %f     Position: %f\n', fn_sorted(i,4),fn_sorted(i,1))

    end
    
    fprintf('-------------------------------------------------------------\n')
    fprintf('Minimal Wear Weapons:\n')
    for i = 1:quan_mw
        fprintf('ID:  %f     Position: %f\n', mw_sorted(i,4),mw_sorted(i,1))

    end
end
