function [ weapons_sorted ] = get_sorted_items( filename )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% weapons = csvread(filename,1,0,[1, 0, nr_of_weapons, 3]);
weapons_table = readtable(filename,'Delimiter',',','ReadVariableNames',true);
weapons_table(:,[5,6])=[];
weapons = table2array(weapons_table);


% Save weight value of weapons
mean_price = mean(weapons(:,3));
weapons(:,5) = 10*weapons(:,2)+(weapons(:,3)-mean_price)/mean_price;


% Sorting after...
sort = 2; % Float
sort = 3; % Price
sort = 5; % combination of both

weapons_sorted = sortrows(weapons, sort);

end

